# README #

TVShowChallenge is a simple android test


### What is this repository for? ###

* Quick summary

Android test as a prove I can develop something interesting

* Version 

Version 1.0

### How do I get set up? ###

* Summary of set up

All the RunConfigurations for Android Studio are shared on this repo, should be imported:

    * API_tests.xml
    * UI_Tests.xml
    * Unit_tests.xml
    * app.xml

Please, use it to run each action

* Dependencies

All the dependencies are configured under gradle, just clean and build the project

* How to run tests

Use the RunConfigurations:

    * API_tests.xml
    * UI_Tests.xml
    * Unit_tests.xml

Please, use it to test each app part

* Deployment instructions

The project includes the signing keystore

### Architecture ###

Based on MVP a structure, the project separates the UI from the datasources. We have:

  * `datasource` package -> api + repository implementations
  * `ui` package -> ui classes (on package for screen)
  * Each screen have `dagger` package, an activity/fragment and a presenter

 Language and other info

This project is developed in kotlin and also some functional programing like

  * Map and filter objects in order to run faster mappers
  * Use lambdas as a callbacks


 Patterns used

  * Repository pattern: used to issolate the server calls and in the future can be used to load local data (like favourite tv shows, etc)
  * Dependency injection: implemented with dagger2

  Testing

  * The UI testing checks the navigation between the screens, also checks all the required elements exists
  * The Unit testing check the data is correct. In this example, the internal mappings work fine
  * Also the API is tested with the repository (methods and structure)

### Who do I talk to? ###

* Repo owner or admin

Repo owner: demenorca@gmail.com or ismael@florit.es

* Other community or team contact

