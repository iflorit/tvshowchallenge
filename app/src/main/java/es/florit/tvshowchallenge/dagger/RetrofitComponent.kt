package es.florit.tvshowchallenge.dagger

import dagger.Component
import es.florit.tvshowchallenge.datasource.repository.TheMovieDBRepository
import javax.inject.Singleton


/**
 * Created by ismael on 19/3/18.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, RetrofitModule::class))
interface RetrofitComponent {

    fun inject(movieDBRepository: TheMovieDBRepository)
}