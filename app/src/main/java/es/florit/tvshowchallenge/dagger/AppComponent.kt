package es.florit.tvshowchallenge.dagger

import dagger.Component
import es.florit.tvshowchallenge.TVShowApplication
import es.florit.tvshowchallenge.datasource.repository.dagger.TheMovieDBModule
import es.florit.tvshowchallenge.ui.detail.dagger.TVShowDetailComponent
import es.florit.tvshowchallenge.ui.detail.dagger.TVShowDetailModule
import es.florit.tvshowchallenge.ui.list.dagger.TVShowListComponent
import es.florit.tvshowchallenge.ui.list.dagger.TVShowListModule
import javax.inject.Singleton

/**
 * Created by ismael on 19/3/18.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, RetrofitModule::class, TheMovieDBModule::class))
interface AppComponent {
    fun inject(app: TVShowApplication)

    fun plus(module: TVShowListModule): TVShowListComponent

    fun plus(module: TVShowDetailModule): TVShowDetailComponent

    fun plus(module: TheMovieDBModule): TheMovieDBModule
}