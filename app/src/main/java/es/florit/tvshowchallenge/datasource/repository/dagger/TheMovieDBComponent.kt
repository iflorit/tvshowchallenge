package es.florit.tvshowchallenge.datasource.repository.dagger

import dagger.Component
import es.florit.tvshowchallenge.ui.base.BasePresenter
import javax.inject.Singleton

/**
 * Created by ismael on 19/3/18.
 */
@Singleton
@Component(modules = arrayOf(TheMovieDBModule::class))
interface TheMovieDBComponent {
    fun inject(tvShowListPresenter: BasePresenter) {}
}
