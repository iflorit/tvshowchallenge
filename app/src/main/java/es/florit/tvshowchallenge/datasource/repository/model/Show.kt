package es.florit.tvshowchallenge.datasource.repository.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * UI data model
 *
 * Implements Parcelable for passing the object between activities (because in this
 * example we don't use local storage)
 *
 * Created by ismael on 17/3/18.
 */
data class Show(
        // tv show list nested fields
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("poster_path") val poster: String,
        @SerializedName("vote_average") val average: Float,

        // tv show detail nested fields
        @SerializedName("overview") val overview: String,
        @SerializedName("backdrop_path") val backdrop : String
) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readFloat(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(poster)
        parcel.writeFloat(average)
        parcel.writeString(overview)
        parcel.writeString(backdrop)
    }

    override fun describeContents(): Int {
        return id
    }

    companion object CREATOR : Parcelable.Creator<Show> {
        override fun createFromParcel(parcel: Parcel): Show {
            return Show(parcel)
        }

        override fun newArray(size: Int): Array<Show?> {
            return arrayOfNulls(size)
        }
    }
}