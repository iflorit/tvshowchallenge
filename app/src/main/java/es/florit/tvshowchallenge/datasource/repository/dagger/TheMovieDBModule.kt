package es.florit.tvshowchallenge.datasource.repository.dagger

import dagger.Module
import dagger.Provides
import es.florit.tvshowchallenge.datasource.repository.TheMovieDBRepository

/**
 * Created by ismael on 19/3/18.
 */

@Module
open class TheMovieDBModule {

    @Provides
    open fun provideRepository(): TheMovieDBRepository {
        return TheMovieDBRepository()
    }
}