package es.florit.tvshowchallenge.datasource.repository

import android.util.Log
import es.florit.tvshowchallenge.dagger.DaggerRetrofitComponent
import es.florit.tvshowchallenge.dagger.RetrofitComponent
import es.florit.tvshowchallenge.dagger.RetrofitModule
import es.florit.tvshowchallenge.datasource.api.model.ShowListResponse
import es.florit.tvshowchallenge.datasource.api.service.TheMovieDBService
import es.florit.tvshowchallenge.datasource.repository.mapper.ShowResponseMapper
import es.florit.tvshowchallenge.datasource.repository.model.Show
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

/**
 * Manager that implements the repository pattern.
 *
 * In this example, there is only one data source so the implementation should be a little
 * useless, but usefull in next steps.
 *
 * Created by ismael on 17/3/18.
 */
class TheMovieDBRepository() {

    val component: RetrofitComponent by lazy {
        DaggerRetrofitComponent
                .builder()
                .retrofitModule(RetrofitModule())
                .build()
    }

    init {
        // do the injection and get an instance of the api service
        component.inject(this)
    }

    @Inject
    lateinit var api: TheMovieDBService

    private val TAG: String = "REPO::"

    /**
     * Returns a page of the remote endpoint tv/populars
     */
    fun getShows(page: Int = 1,
                 onSuccess: (List<Show>) -> Unit,
                 onFailure: (Throwable?) -> Unit = { error -> Log.w("${TAG} getShows", error.toString()) }) {

        // TODO load from local if necessary (not specified)

        // load from remote
        val call = api.popular(page)
        requestShows(call, onSuccess, onFailure)
    }

    /**
     * Returns a page of the remote endpoint tv/similars
     */
    fun getSimilars(similarTo: Int,
                    page: Int = 1,
                    onSuccess: (List<Show>) -> Unit,
                    onFailure: (Throwable?) -> Unit = { error -> Log.w("${TAG} getSimilars", error.toString()) }) {

        // load from remote
        val call = api.similar(similarTo, page)
        requestShows(call, onSuccess, onFailure)
    }

    /**
     * Requests and parses some endpoints (in this case, tv/populars and tv/similars. Both with
     * the same data structure)
     */
    private fun requestShows(call: Call<ShowListResponse>, onSuccess: (List<Show>) -> Unit, onFailure: (Throwable?) -> Unit) {
        call.enqueue(object : Callback<ShowListResponse> {
            override fun onFailure(call: Call<ShowListResponse>?, t: Throwable?) {
                onFailure(t)
            }

            override fun onResponse(call: Call<ShowListResponse>?, response: Response<ShowListResponse>?) {
                /*
                 * read amb map the response into a data structure (both, remote and view layer, have
                 * the same structure, but we separate in order to allow data extension, load data from
                 * other sources, etc)
                 */
                val shows: List<Show> = response?.body()?.shows?.mapNotNull(ShowResponseMapper.mapToShow())
                        ?: listOf()
                onSuccess(shows)
            }
        })
    }
}
