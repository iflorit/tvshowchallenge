package es.florit.tvshowchallenge.datasource.api.model

import com.google.gson.annotations.SerializedName

/**
 * Datamodel containing the responses used on tv/popular and tv/similar endpoints
 *
 * Created by ismael on 17/3/18.
 */
data class ShowListResponse(
        @SerializedName("page") val page: Int,
        @SerializedName("total_results") val results: Int,
        @SerializedName("total_pages") val pages: Int,
        @SerializedName("results") val shows: List<ShowResponse>
)