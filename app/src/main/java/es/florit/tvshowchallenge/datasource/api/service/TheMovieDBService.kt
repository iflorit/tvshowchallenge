package es.florit.tvshowchallenge.datasource.api.service

import es.florit.tvshowchallenge.datasource.api.model.ShowListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Retrofit service, generates the API methods
 *
 * Created by ismael on 17/3/18.
 */
interface TheMovieDBService {

    @GET("tv/popular")
    fun popular(@Query("page") page: Int = 1): Call<ShowListResponse>

    @GET("tv/{tv_id}/similar")
    fun similar(@Path("tv_id") similarTo: Int, @Query("page") page: Int = 1): Call<ShowListResponse>
}