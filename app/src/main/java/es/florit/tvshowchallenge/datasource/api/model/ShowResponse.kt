package es.florit.tvshowchallenge.datasource.api.model

import com.google.gson.annotations.SerializedName

/**
 * Datamodel part of ShowListResponse
 *
 * Created by ismael on 17/3/18.
 */
data class ShowResponse(
        // tv show list nested fields
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("poster_path") val poster: String?,
        @SerializedName("vote_average") val average: Float,

        // tv show detail nested fields
        @SerializedName("overview") val overview: String,
        @SerializedName("original_language") val lang: String,
        @SerializedName("backdrop_path") val backdrop: String?
)