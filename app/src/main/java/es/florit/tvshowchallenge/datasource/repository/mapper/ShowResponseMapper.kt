package es.florit.tvshowchallenge.datasource.repository.mapper

import es.florit.tvshowchallenge.datasource.api.model.ShowResponse
import es.florit.tvshowchallenge.datasource.repository.model.Show

/**
 * Maps the api object to our UI layer object
 *
 * Should not have nulls or empty values, but the remote is not too strict, so this is
 * because I've provided <code>?: ""</code>. Almost, the null or empty data is not on the main
 * fields (id and name)
 *
 * Created by ismael on 19/3/18.
 */
object ShowResponseMapper {

    fun mapToShow(): (ShowResponse) -> Show = {
        Show(
                it.id,
                it.name,
                it.poster ?: "",
                it.average ?: 0.0F,
                it.overview ?: "",
                it.backdrop ?: ""
        )
    }
}