package es.florit.tvshowchallenge

import android.app.Activity
import android.app.Application
import es.florit.tvshowchallenge.dagger.AppComponent
import es.florit.tvshowchallenge.dagger.AppModule
import es.florit.tvshowchallenge.dagger.DaggerAppComponent

/**
 * Custom application, mainly used in this project to inject the app component
 *
 * Created by ismael on 19/3/18.
 */
open class TVShowApplication : Application() {

    open val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }
}


val Activity.app: TVShowApplication
    get() = application as TVShowApplication