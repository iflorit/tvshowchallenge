package es.florit.tvshowchallenge.ui.base

import es.florit.tvshowchallenge.datasource.repository.TheMovieDBRepository
import es.florit.tvshowchallenge.datasource.repository.dagger.DaggerTheMovieDBComponent
import es.florit.tvshowchallenge.datasource.repository.dagger.TheMovieDBModule
import javax.inject.Inject

/**
 * Basic presenter flow.
 *
 * Also injects the db repository
 *
 * Created by ismael on 19/3/18.
 */
abstract class BasePresenter {

    @Inject
    lateinit var movieDBRepository: TheMovieDBRepository

    val component by lazy { DaggerTheMovieDBComponent
            .builder()
            .theMovieDBModule(TheMovieDBModule())
            .build()}

    open fun onCreate() {
        component.inject(this)
    }

    abstract fun onResume()
}