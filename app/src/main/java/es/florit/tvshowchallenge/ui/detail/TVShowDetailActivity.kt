package es.florit.tvshowchallenge.ui.detail

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import es.florit.tvshowchallenge.R
import es.florit.tvshowchallenge.app
import es.florit.tvshowchallenge.datasource.repository.model.Show
import es.florit.tvshowchallenge.ui.base.BaseActivity
import es.florit.tvshowchallenge.ui.base.BasePresenter
import es.florit.tvshowchallenge.ui.detail.dagger.TVShowDetailModule
import es.florit.tvshowchallenge.ui.list.TVShowListActivity
import kotlinx.android.synthetic.main.activity_tvshow_detail.*
import javax.inject.Inject

/**
 * An activity representing a single TVShow detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [TVShowListActivity].
 */
class TVShowDetailActivity : BaseActivity(), TVShowDetailPresenter.ITVShowDetailView {

    val component by lazy { app.component.plus(TVShowDetailModule(this)) }

    @Inject
    lateinit var mPresenter: TVShowDetailPresenter

    lateinit private var fragment: TVShowDetailFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tvshow_detail)

        setSupportActionBar(detail_toolbar)

        // Show the Up button in the action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        restoreFragment(savedInstanceState)

        component.inject(this)

        initPresenter()
    }

    private fun restoreFragment(savedInstanceState: Bundle?) {

        val extra: Show = intent.getParcelableExtra(TVShowDetailFragment.ARG_ITEM)
                ?: savedInstanceState?.getParcelable(TVShowDetailFragment.ARG_ITEM)!!

        fragment = TVShowDetailFragment().apply {
            arguments = Bundle().apply {
                putParcelable(TVShowDetailFragment.ARG_ITEM, extra)
            }
        }

        supportFragmentManager.beginTransaction()
                .replace(R.id.tvshow_detail_container, fragment)
                .commit()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)


        restoreFragment(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)

        outState?.putAll(intent.extras)
    }

    override fun getPresenter(): BasePresenter = mPresenter

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    navigateUpTo(Intent(this, TVShowListActivity::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }


    override fun getEndlessListener() = fragment.getEndlessListener()

    override fun setTVShows(mTVShowSimilarList: MutableList<Show>) {
        fragment.setTVShows(mTVShowSimilarList)
    }

    override fun addTVShows(i: Int, size: Int) {
        fragment.addTVShows(i, size)
    }

    override fun showInternetError() {
        fragment.showInternetError()
    }

    override fun showEmpty(reason: Int) {
        fragment.showEmpty(reason)
    }
}
