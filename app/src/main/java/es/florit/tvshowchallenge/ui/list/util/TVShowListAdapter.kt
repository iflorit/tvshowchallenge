package es.florit.tvshowchallenge.ui.list.util

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.util.Pair
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import es.florit.tvshowchallenge.R
import es.florit.tvshowchallenge.dagger.RetrofitModule
import es.florit.tvshowchallenge.datasource.repository.model.Show
import es.florit.tvshowchallenge.ui.detail.TVShowDetailActivity
import es.florit.tvshowchallenge.ui.detail.TVShowDetailFragment
import es.florit.tvshowchallenge.ui.list.TVShowListActivity
import kotlinx.android.synthetic.main.tvshow_list_content.view.*


/**
 * Created by ismael on 18/3/18.
 */
class TVShowListAdapter(private val mParentActivity: TVShowListActivity,
                        private val mValues: MutableList<Show>,
                        private val mTwoPane: Boolean) :
        RecyclerView.Adapter<TVShowListAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Show
            if (mTwoPane) {
                val fragment = TVShowDetailFragment().apply {
                    arguments = Bundle().apply {
                        // putInt(TVShowDetailFragment.ARG_ITEM_ID, item.id)
                        putParcelable(TVShowDetailFragment.ARG_ITEM, item)
                    }
                }
                mParentActivity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.tvshow_detail_container, fragment)
                        .commit()
            } else {
                val intent = Intent(v.context, TVShowDetailActivity::class.java).apply {
                    putExtra(TVShowDetailFragment.ARG_ITEM, item)
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    val posterTransition: Pair<View, String> = Pair(v.card_poster.poster, v.context.getString(R.string.transition_poster))
                    /*
                    val nameTransition: Pair<View, String> = Pair(v.card_view.name, v.context.getString(R.string.transition_title))
                    val overviewTransition: Pair<View, String> = Pair(v.card_view.overview, v.context.getString(R.string.transition_overview))
                    val cardTransition: Pair<View, String> = Pair(v.card_view, v.context.getString(R.string.transition_card))
                    */
                    v.context.startActivity(intent,
                            ActivityOptionsCompat.makeSceneTransitionAnimation(mParentActivity,
                                    posterTransition
                            ).toBundle())
                } else {
                    v.context.startActivity(intent)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tvshow_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mNameView.text = item.name
        holder.mOverviewView.text = item.overview
        holder.mRating.rating = item.average / 2

        Glide.with(mParentActivity)
                .load("${RetrofitModule.BASE_IMG}${item.poster}")
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                .apply(RequestOptions
                        .placeholderOf(R.drawable.poster_placeholder)
                        .centerCrop())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.mPoster)

        with(holder.itemView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }


        setAnimation(holder.itemView, position);
    }


    private var lastPosition = -1
    private fun setAnimation(viewToAnimate: View, position: Int) {
        val animation = AnimationUtils.loadAnimation(mParentActivity,
                if (position > lastPosition)
                    R.anim.up_from_bottom
                else
                    R.anim.down_from_top)
        viewToAnimate.startAnimation(animation)
        lastPosition = position
    }


    override fun getItemCount(): Int {
        return mValues.size
    }

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val mCardPoster: CardView = mView.card_poster
        val mPoster: ImageView = mView.poster

        val mCard: CardView = mView.card_view
        val mNameView: TextView = mView.name
        val mOverviewView: TextView = mView.overview
        val mRating: RatingBar = mView.rating
    }
}