package es.florit.tvshowchallenge.ui.list.dagger

import dagger.Subcomponent
import es.florit.tvshowchallenge.dagger.RetrofitModule
import es.florit.tvshowchallenge.ui.list.TVShowListActivity

/**
 * Created by ismael on 19/3/18.
 */
@Subcomponent(modules = arrayOf(RetrofitModule::class,TVShowListModule::class))
interface TVShowListComponent {
    fun inject(activity: TVShowListActivity)

}
