package es.florit.tvshowchallenge.ui.list

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.bumptech.glide.Glide
import es.florit.tvshowchallenge.R
import es.florit.tvshowchallenge.app
import es.florit.tvshowchallenge.datasource.repository.model.Show
import es.florit.tvshowchallenge.ui.base.BaseActivity
import es.florit.tvshowchallenge.ui.base.BasePresenter
import es.florit.tvshowchallenge.ui.list.dagger.TVShowListModule
import es.florit.tvshowchallenge.ui.list.util.EndlessOnScrollListener
import es.florit.tvshowchallenge.ui.list.util.TVShowListAdapter
import kotlinx.android.synthetic.main.activity_tvshow_list.*
import kotlinx.android.synthetic.main.tvshow_list.*
import javax.inject.Inject

/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [TVShowDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class TVShowListActivity : BaseActivity(), TVShowListPresenter.ITVShowListView {

    val component by lazy { app.component.plus(TVShowListModule(this)) }

    @Inject
    lateinit var mPresenter: TVShowListPresenter

    lateinit var mEndlessListener: EndlessOnScrollListener

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var mTwoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tvshow_list)

        component.inject(this)

        setSupportActionBar(toolbar)
        toolbar.title = title

        if (tvshow_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true
        }

        initViews()
        initPresenter()
    }

    override fun getPresenter(): BasePresenter = mPresenter

    private fun initViews() {
        mEndlessListener = object : EndlessOnScrollListener(LinearLayoutManager(this@TVShowListActivity)) {
            override fun onLoadMore(currentPage: Int) {
                mPresenter.onLoadMore(currentPage)
            }
        }

        tvshow_list.addOnScrollListener(mEndlessListener)

        retry.setOnClickListener({ b ->
            mPresenter.onResume()
        })
    }

    override fun getEndlessListener() = mEndlessListener


    override fun setTVShows(mTVShowList: MutableList<Show>) {
        tvshow_list.setHasFixedSize(true)
        tvshow_list.setLayoutManager(GridLayoutManager(this@TVShowListActivity, 1))
        tvshow_list.adapter = TVShowListAdapter(this, mTVShowList, mTwoPane)

        tvshow_list.visibility = View.VISIBLE
        empty.visibility = View.GONE
    }

    override fun addTVShows(insertIndex: Int, insertCount: Int) {
        tvshow_list.adapter.notifyItemRangeInserted(insertIndex, insertCount);
    }

    override fun showInternetError() {
        showEmpty(R.drawable.offline)
    }

    override fun showEmpty(emptyReason: Int) {
        tvshow_list.visibility = View.GONE
        empty.visibility = View.VISIBLE
        Glide.with(this).load(emptyReason).into(iv_empty)
    }

}
