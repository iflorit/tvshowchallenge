package es.florit.tvshowchallenge.ui.list.dagger

import dagger.Module
import dagger.Provides
import es.florit.tvshowchallenge.ui.list.TVShowListActivity
import es.florit.tvshowchallenge.ui.list.TVShowListPresenter

/**
 * Created by ismael on 19/3/18.
 */
@Module
class TVShowListModule(val activity: TVShowListActivity) {

    @Provides
    fun providePresenter(): TVShowListPresenter {
        return TVShowListPresenter(activity)
    }
}