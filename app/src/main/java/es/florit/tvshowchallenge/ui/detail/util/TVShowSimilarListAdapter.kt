package es.florit.tvshowchallenge.ui.detail.util

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import es.florit.tvshowchallenge.R
import es.florit.tvshowchallenge.dagger.RetrofitModule
import es.florit.tvshowchallenge.datasource.repository.model.Show
import es.florit.tvshowchallenge.ui.detail.TVShowDetailActivity
import es.florit.tvshowchallenge.ui.detail.TVShowDetailFragment
import kotlinx.android.synthetic.main.tvshow_similar_content.view.*


/**
 * Simple adapter used on similar tvshows
 *
 * Created by ismael on 18/3/18.
 */
class TVShowSimilarListAdapter(private val mParentActivity: TVShowDetailActivity,
                               private val mValues: MutableList<Show>) :
        RecyclerView.Adapter<TVShowSimilarListAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener = View.OnClickListener { v ->
        val item = v.tag as Show
        val intent = Intent(v.context, TVShowDetailActivity::class.java).apply {
            putExtra(TVShowDetailFragment.ARG_ITEM, item)
        }
        v.context.startActivity(intent)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tvshow_similar_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        Glide.with(mParentActivity)
                .load("${RetrofitModule.BASE_IMG}${item.poster}")
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                .into(holder.mPoster)

        with(holder.itemView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount() = mValues.size

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val mPoster: ImageView = mView.tvshow_similar_poster

    }
}