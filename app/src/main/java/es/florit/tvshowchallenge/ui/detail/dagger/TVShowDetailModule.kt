package es.florit.tvshowchallenge.ui.detail.dagger

import dagger.Module
import dagger.Provides
import es.florit.tvshowchallenge.ui.detail.TVShowDetailActivity
import es.florit.tvshowchallenge.ui.detail.TVShowDetailPresenter

/**
 * Created by ismael on 19/3/18.
 */
@Module
class TVShowDetailModule(val activity: TVShowDetailActivity) {

    @Provides
    fun providePresenter(): TVShowDetailPresenter {
        return TVShowDetailPresenter(activity)
    }
}