package es.florit.tvshowchallenge.ui.list

import es.florit.tvshowchallenge.R
import es.florit.tvshowchallenge.datasource.repository.model.Show
import es.florit.tvshowchallenge.ui.base.BasePresenter
import es.florit.tvshowchallenge.ui.list.util.EndlessOnScrollListener
import java.io.IOException

/**
 * Presenter that manages all the login on this screen
 *
 * Created by ismael on 18/3/18.
 */
class TVShowListPresenter(val mView: ITVShowListView) : BasePresenter() {

    private val TAG = "TVShowListPresenter"

    val mTVShowList: MutableList<Show> by lazy { arrayListOf<Show>() }


    override fun onCreate() {
        super.onCreate()

        mView.getEndlessListener().isLoading = false
    }

    override fun onResume() {
        if (mTVShowList.isEmpty()) {
            mView.getEndlessListener().onLoadMore(0)
        }
    }

    /**
     * MVP interface that should follow the activity/fragment
     */
    interface ITVShowListView {
        /** returns the scroll tracker */
        fun getEndlessListener(): EndlessOnScrollListener

        /** sets the init amount of tv shows on the similars list */
        fun setTVShows(mTVShowList: MutableList<Show>)

        /** adds one more page to the similars list */
        fun addTVShows(insertIndex: Int, insertCount: Int)

        /** reports a connection error */
        fun showInternetError()

        /** reports an empty list */
        fun showEmpty(reason: Int = R.drawable.empty)
    }

    fun onLoadMore(currentPage: Int) {
        getMore(currentPage)
    }

    /**
     * Loads more similar tvshows (should put the page that we would load)
     */
    private fun getMore(currentPage: Int) {
        // TODO cancelCall()
        mView.getEndlessListener().isLoading = true
        //mView.addFooter(HustlerListAdapter.FOOTER_PROGRESS);

        movieDBRepository.getShows(
                page = currentPage + 1,
                onSuccess = { result ->
                    // what to do after load more results
                    mView.getEndlessListener().isLoading = false

                    if (mTVShowList.isEmpty()) {
                        mTVShowList.addAll(result)
                        mView.setTVShows(mTVShowList)
                    } else if (!result.isEmpty()) {
                        mTVShowList.addAll(result)
                        mView.addTVShows(mTVShowList.size - result.size, result.size)
                    }
                },
                onFailure = { result ->
                    when (result) {
                        is IOException -> mView.showInternetError()
                        else -> mView.showEmpty()
                    }
                }
        )
    }
}
