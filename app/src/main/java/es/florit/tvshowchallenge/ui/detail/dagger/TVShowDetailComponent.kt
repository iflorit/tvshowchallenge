package es.florit.tvshowchallenge.ui.detail.dagger

import dagger.Subcomponent
import es.florit.tvshowchallenge.dagger.AppModule
import es.florit.tvshowchallenge.ui.detail.TVShowDetailActivity
import es.florit.tvshowchallenge.ui.detail.TVShowDetailFragment

/**
 * Created by ismael on 19/3/18.
 */
@Subcomponent(modules = arrayOf(AppModule::class,TVShowDetailModule::class))
interface TVShowDetailComponent {
    fun inject(activity: TVShowDetailActivity)

    fun inject(fragment: TVShowDetailFragment)
}
