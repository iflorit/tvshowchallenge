package es.florit.tvshowchallenge.ui.detail

import es.florit.tvshowchallenge.datasource.repository.model.Show
import es.florit.tvshowchallenge.ui.base.BasePresenter
import es.florit.tvshowchallenge.ui.list.TVShowListPresenter
import java.io.IOException

/**
 * Presenter that manages all the login on this screen
 *
 * Created by ismael on 19/3/18.
 */
class TVShowDetailPresenter(val mView: ITVShowDetailView) : BasePresenter() {

    val mTVShowSimilarList: MutableList<Show> by lazy { arrayListOf<Show>() }

    override fun onCreate() {
        super.onCreate()
    }

    override fun onResume() {
        if (mTVShowSimilarList.isEmpty()) {
            mView.getEndlessListener().onLoadMore(0)
        }
    }

    /**
     * MVP interface that should follow the activity/fragment
     *
     * The detail interface is similar than the main list, but the
     * endless scroll tracker is used in a different way (as a secondary
     * functionality)
     */
    interface ITVShowDetailView : TVShowListPresenter.ITVShowListView {}

    fun onLoadMore(similarTo: Int, currentPage: Int) {
        getMore(similarTo, currentPage)
    }

    /**
     * Loads more similar tvshows (should put the page that we would load)
     */
    private fun getMore(similarTo: Int, currentPage: Int) {
        mView.getEndlessListener().isLoading = true

        movieDBRepository.getSimilars(
                similarTo = similarTo,
                page = currentPage + 1,
                onSuccess = { result ->
                    // what to do after load more results
                    mView.getEndlessListener().isLoading = false

                    if (mTVShowSimilarList.isEmpty()) {
                        mTVShowSimilarList.addAll(result)
                        mView.setTVShows(mTVShowSimilarList)
                    } else if (!result.isEmpty()) {
                        mTVShowSimilarList.addAll(result)
                        mView.addTVShows(mTVShowSimilarList.size - result.size, result.size)
                    }
                },
                onFailure = { result ->
                    when (result) {
                        is IOException -> mView.showInternetError()
                        else -> mView.showEmpty()
                    }
                }
        )
    }
}