package es.florit.tvshowchallenge.ui.detail

import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import es.florit.tvshowchallenge.R
import es.florit.tvshowchallenge.app
import es.florit.tvshowchallenge.dagger.RetrofitModule
import es.florit.tvshowchallenge.datasource.repository.model.Show
import es.florit.tvshowchallenge.ui.detail.dagger.TVShowDetailModule
import es.florit.tvshowchallenge.ui.detail.util.TVShowSimilarListAdapter
import es.florit.tvshowchallenge.ui.list.util.EndlessOnScrollListener
import kotlinx.android.synthetic.main.activity_tvshow_detail.*
import kotlinx.android.synthetic.main.tvshow_detail.*
import kotlinx.android.synthetic.main.tvshow_detail.view.*
import javax.inject.Inject

/**
 * A fragment representing a single TVShow detail screen.
 * This fragment is either contained in a [TVShowListActivity]
 * in two-pane mode (on tablets) or a [TVShowDetailActivity]
 * on handsets.
 */
class TVShowDetailFragment : Fragment(), TVShowDetailPresenter.ITVShowDetailView {

    companion object {
        /**
         * arg item name for pass the Show object between activities
         */
        const val ARG_ITEM = "item"
    }

    val component by lazy { activity?.app?.component?.plus(TVShowDetailModule(activity as TVShowDetailActivity)) }

    @Inject
    lateinit var mPresenter: TVShowDetailPresenter

    lateinit private var mItem: Show
    lateinit private var mEndlessListener: EndlessOnScrollListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_ITEM)) {
                mItem = it.getParcelable(ARG_ITEM)

                activity?.let {
                    it.toolbar_layout?.title = mItem.name

                    it.background_toolbar?.let { backdrop ->
                        Glide.with(it).load("${RetrofitModule.BASE_IMG}${mItem.backdrop}")
                                .into(backdrop)
                    };
                }
            }
        }

        component?.inject(this)
        mPresenter.onCreate()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.tvshow_detail, container, false)

        mItem?.let { show ->
            // "show" not required, but clarifies the code

            rootView.name.text = show.name
            rootView.overview.text = show.overview
            rootView.rating.rating = show.average / 2

            activity?.let {
                // only starts the image download with a valid activity
                Glide.with(it).load("${RetrofitModule.BASE_IMG}${mItem.poster}")
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                        .apply(RequestOptions
                                .placeholderOf(R.drawable.poster_placeholder)
                                .centerCrop())
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(rootView.poster)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    rootView.poster.transitionName = getString(R.string.transition_poster);
                }
            }

            /*
             * prepare all the stuff required for the similars horizontal scroll view
             */
            mEndlessListener = object : EndlessOnScrollListener(LinearLayoutManager(activity)) {
                override fun onLoadMore(currentPage: Int) {
                    mPresenter.onLoadMore(similarTo = show.id, currentPage = currentPage)
                }
            }

            rootView.tvshow_list_similars.addOnScrollListener(mEndlessListener)
        }

        return rootView
    }

    override fun onResume() {
        super.onResume()

        mPresenter.onResume()
    }

    override fun setTVShows(mTVShowList: MutableList<Show>) {
        tvshow_list_similars?.let {
            it.setHasFixedSize(true)
            it.setLayoutManager(GridLayoutManager(activity, 1, GridLayoutManager.HORIZONTAL, false))
            it.adapter = TVShowSimilarListAdapter(activity as TVShowDetailActivity, mTVShowList)

            it.visibility = View.VISIBLE
        }
    }

    override fun addTVShows(insertIndex: Int, insertCount: Int) {
        tvshow_list_similars.adapter.notifyItemRangeInserted(insertIndex, insertCount);
    }

    override fun showInternetError() {
        showEmpty()
    }

    /**
     * In this case, only hides the similar collection, not shows "no internet" options
     */
    override fun showEmpty(emptyReason: Int) {
        tvshow_list_similars.visibility = View.GONE
    }


    override fun getEndlessListener() = mEndlessListener
}
