package es.florit.tvshowchallenge.datasource.repository.mapper

import es.florit.tvshowchallenge.datasource.api.model.ShowResponse
import junit.framework.Assert
import org.junit.Test

/**
 * Created by ismael on 20/3/18.
 */
class ShowResponseMapperTest {
    @Test
    fun mapToShow() {

        val showsResponse = mutableListOf<ShowResponse>()

        showsResponse.add(ShowResponse(id = 1, name = "Name 1", poster = "path/poster/1.png", average = 1.0F, overview = "Overview 1", lang = "en", backdrop = "path/backdrop/1.png"))
        showsResponse.add(ShowResponse(id = 2, name = "Name 2", poster = "path/poster/2.png", average = 2.0F, overview = "Overview 2", lang = "en", backdrop = "path/backdrop/2.png"))

        val showsEntities = showsResponse.map(ShowResponseMapper.mapToShow())

        for (i in 0..showsResponse.size-1) {
            Assert.assertEquals(showsResponse.get(i).id, showsEntities.get(i).id)
            Assert.assertEquals(showsResponse.get(i).name, showsEntities.get(i).name)
            Assert.assertEquals(showsResponse.get(i).poster, showsEntities.get(i).poster)
            Assert.assertEquals(showsResponse.get(i).average, showsEntities.get(i).average)
            Assert.assertEquals(showsResponse.get(i).overview, showsEntities.get(i).overview)
            Assert.assertEquals(showsResponse.get(i).backdrop, showsEntities.get(i).backdrop)
        }

    }
}