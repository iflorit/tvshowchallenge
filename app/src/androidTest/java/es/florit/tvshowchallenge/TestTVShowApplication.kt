
package es.florit.tvshowchallenge.datasource

import es.florit.tvshowchallenge.TVShowApplication
import es.florit.tvshowchallenge.dagger.*
import es.florit.tvshowchallenge.datasource.repository.dagger.TheMovieDBModule

class TestTVShowApplication : TVShowApplication() {

    override val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .theMovieDBModule(MockTheMovieDBModule())
                .retrofitModule(RetrofitModule())
                .build()
    }

    override fun onCreate() {
        component.inject(this)

        component.plus(TheMovieDBModule())
    }
}