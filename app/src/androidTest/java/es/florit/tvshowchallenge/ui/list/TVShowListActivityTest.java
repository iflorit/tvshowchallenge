package es.florit.tvshowchallenge.ui.list;


import android.os.SystemClock;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import javax.inject.Inject;

import es.florit.tvshowchallenge.R;
import es.florit.tvshowchallenge.dagger.DaggerMockAppComponent;
import es.florit.tvshowchallenge.dagger.MockTheMovieDBModule;
import es.florit.tvshowchallenge.datasource.repository.TheMovieDBRepository;
import es.florit.tvshowchallenge.datasource.repository.model.Show;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class TVShowListActivityTest {

    @Rule
    public ActivityTestRule<TVShowListActivity> mActivityTestRule = new ActivityTestRule<>(TVShowListActivity.class);

    @Inject
    public TheMovieDBRepository movieDBRepository;

    /**
     * Testeable shows
     */
    private List<Show> firstShows;


    @Before
    public void setUp() {

        DaggerMockAppComponent
                .builder()
                .theMovieDBModule(new MockTheMovieDBModule())
                .build()
                .test(this);



        movieDBRepository.getShows(1, new Function1<List<Show>, Unit>() {
            @Override
            public Unit invoke(List<Show> shows) {
                firstShows = shows;

                return null;
            }
        }, new Function1<Throwable, Unit>() {
            @Override
            public Unit invoke(Throwable throwable) {
                return null;
            }
        });
    }


    /**
     * Check all required elems are shown
     */
    @Test
    public void test1RequiredUIElements() {
        SystemClock.sleep(5000); // sleep for loading purposes

        Show testedShow = firstShows.get(0);

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.tvshow_list),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.frameLayout),
                                        0),
                                0),
                        isDisplayed()));
        recyclerView.check(matches(isDisplayed()));

        // title
        ViewInteraction textView = onView((allOf(
                withId(R.id.name),
                withText(testedShow.getName()),
                childAtPosition(childAtPosition(withId(R.id.card_view), 0), 0),
                isDisplayed()
        )));
        textView.check(matches(isDisplayed()));


        // ratings
        ViewInteraction ratingBar = onView(first(allOf(
                withId(R.id.rating),
                childAtPosition(
                        childAtPosition(
                                IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                2),
                        1),
                isDisplayed())));
        ratingBar.check(matches(isDisplayed()));

        // poster
        ViewInteraction imageView = onView(first(allOf(
                withId(R.id.poster),
                childAtPosition(
                        allOf(withId(R.id.card_poster),
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.view.View.class),
                                        0)),
                        0),
                isDisplayed())));
        imageView.check(matches(isDisplayed()));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }


    protected <T> Matcher<T> first(final Matcher<T> matcher) {
        return new BaseMatcher<T>() {
            boolean isFirst = true;

            @Override
            public boolean matches(final Object item) {
                if (isFirst && matcher.matches(item)) {
                    isFirst = false;
                    return true;
                }

                return false;
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("should return first matching item");
            }
        };
    }
}
