package es.florit.tvshowchallenge.ui.detail;


import android.os.SystemClock;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import javax.inject.Inject;

import es.florit.tvshowchallenge.R;
import es.florit.tvshowchallenge.dagger.DaggerMockAppComponent;
import es.florit.tvshowchallenge.dagger.MockTheMovieDBModule;
import es.florit.tvshowchallenge.datasource.repository.TheMovieDBRepository;
import es.florit.tvshowchallenge.datasource.repository.model.Show;
import es.florit.tvshowchallenge.ui.list.TVShowListActivity;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class TVShowDetailActivityTest {

    @Rule
    public ActivityTestRule<TVShowListActivity> mActivityTestRule = new ActivityTestRule<>(TVShowListActivity.class);

    @Inject
    public TheMovieDBRepository movieDBRepository;

    /**
     * Testeable shows
     */
    private List<Show> firstShows;


    @Before
    public void setUp() {

        DaggerMockAppComponent
                .builder()
                .theMovieDBModule(new MockTheMovieDBModule())
                .build()
                .test(this);


        movieDBRepository.getShows(1, new Function1<List<Show>, Unit>() {
            @Override
            public Unit invoke(List<Show> shows) {
                firstShows = shows;

                return null;
            }
        }, new Function1<Throwable, Unit>() {
            @Override
            public Unit invoke(Throwable throwable) {
                return null;
            }
        });
    }

    /**
     * Check all required elems are shown
     */
    @Test
    public void test1RequiredUIElements() {

        SystemClock.sleep(5000); // sleep for loading purposes

        int tapPosition = 0;
        Show testedShow = firstShows.get(tapPosition);

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.tvshow_list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)));
        recyclerView.perform(actionOnItemAtPosition(tapPosition, click()));

        SystemClock.sleep(500); // sleep for loading purposes


        ViewInteraction textView = onView(
                allOf(withId(R.id.name), withText(testedShow.getName()), isDisplayed()));
        textView.check(matches(isDisplayed()));

        ViewInteraction overView = onView(
                allOf(withId(R.id.name), withText(testedShow.getOverview()), isDisplayed()));
        textView.check(matches(isDisplayed()));


        ViewInteraction imageView = onView((
                allOf(withId(R.id.background_toolbar), isDisplayed())));
        imageView.check(matches(isDisplayed()));
    }

    @Test
    public void test1RequiredUISimilarsAndNavigateTo() {

        SystemClock.sleep(5000); // sleep for loading purposes
        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.tvshow_list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)));
        recyclerView.perform(actionOnItemAtPosition(0, click()));

        SystemClock.sleep(5000); // sleep for loading purposes

        ViewInteraction similarsView = onView(withId(R.id.tvshow_list_similars));
        similarsView.perform(actionOnItemAtPosition(0, click()));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
