package es.florit.tvshowchallenge.dagger

import dagger.Component
import es.florit.tvshowchallenge.datasource.api.TheMovieDBAPITest
import es.florit.tvshowchallenge.datasource.repository.TheMovieDBRepositoryTest
import es.florit.tvshowchallenge.datasource.repository.dagger.TheMovieDBModule
import es.florit.tvshowchallenge.ui.detail.TVShowDetailActivityTest
import es.florit.tvshowchallenge.ui.list.TVShowListActivityTest
import javax.inject.Singleton

/**
 * Created by ismael on 20/3/18.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, RetrofitModule::class, TheMovieDBModule::class))
interface MockAppComponent : AppComponent {

    fun test(theMovieDBAPITest: TheMovieDBAPITest) {}

    fun test(theMovieDBAPITest: TheMovieDBRepositoryTest) {}

    fun test(theMovieDBAPITest: TVShowListActivityTest) {}

    fun test(theMovieDBAPITest: TVShowDetailActivityTest) {}

}