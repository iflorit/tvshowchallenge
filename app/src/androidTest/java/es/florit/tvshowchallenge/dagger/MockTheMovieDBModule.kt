package es.florit.tvshowchallenge.dagger

import dagger.Module
import es.florit.tvshowchallenge.datasource.repository.dagger.TheMovieDBModule

/**
 * Created by ismael on 20/3/18.
 */
@Module
class MockTheMovieDBModule: TheMovieDBModule() {

}