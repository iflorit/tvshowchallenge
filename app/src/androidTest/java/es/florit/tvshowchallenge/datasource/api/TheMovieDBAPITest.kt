package es.florit.tvshowchallenge.datasource.api

import android.support.test.runner.AndroidJUnit4
import es.florit.tvshowchallenge.BuildConfig
import es.florit.tvshowchallenge.dagger.DaggerMockAppComponent
import es.florit.tvshowchallenge.dagger.MockAppComponent
import es.florit.tvshowchallenge.dagger.MockTheMovieDBModule
import es.florit.tvshowchallenge.datasource.api.service.TheMovieDBService
import junit.framework.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject


/**
 * Created by ismael on 17/3/18.
 */
@RunWith(AndroidJUnit4::class)
class TheMovieDBAPITest {

    @Inject
    lateinit var api: TheMovieDBService


    open val component: MockAppComponent by lazy {
        DaggerMockAppComponent
                .builder()
                .theMovieDBModule(MockTheMovieDBModule())
                .build()
    }


    @Before
    fun setUp() {

        component.test(this)
    }

    @Test
    fun getApi() {
        Assert.assertNotNull(api)
    }

    @Test
    fun checkApiKey() {
        Assert.assertNotNull(BuildConfig.THEMOVIEDB_API_KEY)

        Assert.assertTrue(BuildConfig.THEMOVIEDB_API_KEY.length > 0)
    }

    @Test
    fun getApiPopulars() {
        val response = api.popular().execute()

        Assert.assertEquals(200, response.code())
        Assert.assertNotNull(response.body())

        val responsePageIndexOutOfBounds = api.popular(response.body()?.pages!! * 10).execute()
        Assert.assertEquals(422, responsePageIndexOutOfBounds.code())
    }

    @Test
    fun getApiSimilars() {
        val response = api.similar(1418).execute()

        Assert.assertEquals(200, response.code())
    }
}