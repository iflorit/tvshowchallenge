package es.florit.tvshowchallenge.datasource.repository

import es.florit.tvshowchallenge.dagger.DaggerMockAppComponent
import es.florit.tvshowchallenge.dagger.MockAppComponent
import es.florit.tvshowchallenge.dagger.MockTheMovieDBModule
import junit.framework.Assert
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

/**
 * Created by ismael on 18/3/18.
 */
class TheMovieDBRepositoryTest {

    @Inject lateinit var movieDBRepository: TheMovieDBRepository


    open val component: MockAppComponent by lazy {
        DaggerMockAppComponent
                .builder()
                .theMovieDBModule(MockTheMovieDBModule())
                .build()
    }


    @Before
    fun setUp() {

        component.test(this)
    }



    @Test
    fun getShows() {
        movieDBRepository.getShows(page = 0,
                onSuccess = {},
                onFailure = { error ->
                    System.out.println("TheMovieDBRepositoryTest::getShows ->" + error);
                })

        movieDBRepository.getShows(page = 1,
                onSuccess = { shows ->
                    Assert.assertNotNull(shows)
                })
    }

    @Test
    fun getSimilars() {
        movieDBRepository.getSimilars(similarTo = 1418, page = 0,
                onSuccess = {},
                onFailure = { error ->
                    System.out.println("TheMovieDBRepositoryTest::getSimilars ->" + error);
                })

        movieDBRepository.getSimilars(similarTo = 1418, page = 1,
                onSuccess = { shows ->
                    Assert.assertNotNull(shows)
                })
    }
}